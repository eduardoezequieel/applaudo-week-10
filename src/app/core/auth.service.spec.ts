import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AuthService } from './auth.service';
import { environment } from 'src/environments/environment';
import { HttpErrorResponse } from '@angular/common/http';
import { Error } from '../core/interfaces/error';

describe('AuthService', () => {
  let service: AuthService;
  let http: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AuthService],
    });

    service = TestBed.inject(AuthService);
    http = TestBed.inject(HttpTestingController);
  });

  it('should log in successfully', () => {
    service.logout();

    const mockResponse = {
      data: {
        token: 'asdjsakdlkajsd',
        user: {
          id: 1,
          email: 'trainee1@example.com',
          name: 'Trainee 1',
        },
      },
    };

    const bodyRequest = {
      data: {
        email: 'trainee1@example.com',
        password: 'Trainee$1',
      },
    };

    service.login(bodyRequest).subscribe({
      next: (response) => {
        expect(response).toBe(mockResponse);
        service.logout();
      },
    });

    const httpRequest = http.expectOne(environment.api + 'api/v1/users/login');
    httpRequest.flush(mockResponse);
  });

  it('should throw an alert of an invalid login', async () => {
    const bodyRequest = {
      data: {
        email: 'trainee1@example.com',
        password: 'pass123',
      },
    };

    const mockError = new HttpErrorResponse({ status: 401, statusText: 'Unauthorized' });

    service.login(bodyRequest).subscribe({
      error: (error: Error) => {
        expect(error.expected).toBeTrue();
      },
    });

    const httpRequest = http.expectOne(environment.api + 'api/v1/users/login');

    httpRequest.flush('', mockError);
  });

  it('should throw an alert of an unknown error', async () => {
    const bodyRequest = {
      data: {
        email: 'trainee1@example.com',
        password: 'pass123',
      },
    };

    const mockError = new HttpErrorResponse({ status: 500, statusText: 'Internal Server Error' });

    service.login(bodyRequest).subscribe({
      error: (error: Error) => {
        expect(error.expected).not.toBeTrue();
      },
    });

    const httpRequest = http.expectOne(environment.api + 'api/v1/users/login');

    httpRequest.flush('', mockError);
  });
});
