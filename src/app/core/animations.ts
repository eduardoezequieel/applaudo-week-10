import { animate, style, transition, trigger } from '@angular/animations';

export const productAnimation = trigger('productAnimation', [
  transition(':enter', [
    style({ opacity: 0, transform: 'scale(0.95)' }),
    animate('0.4s ease-out', style({ opacity: 1, transform: 'scale(1)' })),
  ]),
]);

export const pageAnimation = trigger('pageAnimation', [
  transition(':enter', [
    style({ opacity: 0, transform: 'translate(-100px)' }),
    animate('0.5s ease-in-out', style({ opacity: 1, transform: 'translate(0px)' })),
  ]),
]);
