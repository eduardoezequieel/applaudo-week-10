import { MatButtonHarness } from '@angular/material/button/testing';
import { MatMenuHarness } from '@angular/material/menu/testing';
import { MatDialogHarness } from '@angular/material/dialog/testing';
import { User } from 'src/app/auth/interfaces/user';
import { StorageService } from './../../../core/storage.service';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatMenuModule } from '@angular/material/menu';
import { MatTooltipModule } from '@angular/material/tooltip';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { JwtModule } from '@auth0/angular-jwt';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { appReducers } from 'src/app/app.reducer';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProductsService } from '../../services/products.service';
import { EffectsArray } from '../../store/effects';
import { NavbarComponent } from './navbar.component';

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;
  let loader: HarnessLoader;

  const storageServiceSpy = jasmine.createSpyObj('StorageService', [
    'isTokenExpired',
    'getUserInfo',
  ]);

  const mockUser: User = {
    id: 3,
    name: 'Trainee 1',
    email: 'trainee1@example.com',
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NavbarComponent],
      imports: [
        BrowserAnimationsModule,
        HttpClientTestingModule,
        JwtModule.forRoot({ config: {} }),
        RouterTestingModule,
        StoreModule.forRoot(appReducers),
        EffectsModule.forRoot(EffectsArray),
        SharedModule,
        MatMenuModule,
        MatTooltipModule,
        MatDialogModule,
      ],
      providers: [ProductsService, { provide: StorageService, useValue: storageServiceSpy }],
    }).compileComponents();

    fixture = TestBed.createComponent(NavbarComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;

    storageServiceSpy.isTokenExpired.and.returnValue(false);
    storageServiceSpy.getUserInfo.and.returnValue(mockUser);

    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeDefined();
  });

  it('should open the profile menu only if the user is logged in', async () => {
    expect(component.isLoggedIn).toBeTrue();
    expect(component.user).toBe(mockUser);

    const button = await loader.getHarness(
      MatButtonHarness.with({ selector: "button[matTooltip='Profile']" })
    );

    button.click();

    const menu = await loader.getHarness(MatMenuHarness);

    expect(await menu.isOpen()).toBeTrue();
  });
});
