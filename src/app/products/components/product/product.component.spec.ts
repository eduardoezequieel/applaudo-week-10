import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatMenuModule } from '@angular/material/menu';
import { MatTooltipModule } from '@angular/material/tooltip';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { JwtModule } from '@auth0/angular-jwt';
import { EffectsModule } from '@ngrx/effects';
import { Store, StoreModule } from '@ngrx/store';
import { appReducers, AppState } from 'src/app/app.reducer';
import { StorageService } from 'src/app/core/storage.service';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProductsService } from '../../services/products.service';
import { EffectsArray } from '../../store/effects';
import { ProductComponent } from './product.component';
import * as mockProducts from '../../../test/mock-products.json';
import { Product } from '../../interfaces/product';
import { Meta } from '../../interfaces/meta';
import { By } from '@angular/platform-browser';
import { MatButtonHarness } from '@angular/material/button/testing';
import { EMPTY, of } from 'rxjs';
import { CartDialog } from './cart-dialog';
import * as CartActions from '../../store/actions/cart.actions';
import { AlertService } from 'src/app/shared/services/alert.service';

describe('ProductComponent', () => {
  let component: ProductComponent;
  let fixture: ComponentFixture<ProductComponent>;
  let loader: HarnessLoader;
  let mockData: { data: Product[]; meta: Meta } = (mockProducts as any).default;
  let dialog: MatDialog;
  let store: Store<AppState>;

  const storageServiceSpy = jasmine.createSpyObj('StorageService', ['isTokenExpired']);
  const alertServiceSpy = jasmine.createSpyObj('AlertService', ['notify']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProductComponent],
      imports: [
        BrowserAnimationsModule,
        HttpClientTestingModule,
        JwtModule.forRoot({ config: {} }),
        RouterTestingModule,
        StoreModule.forRoot(appReducers),
        EffectsModule.forRoot(EffectsArray),
        SharedModule,
        MatMenuModule,
        MatTooltipModule,
        MatDialogModule,
      ],
      providers: [
        ProductsService,
        { provide: StorageService, useValue: storageServiceSpy },
        { provide: AlertService, useValue: alertServiceSpy },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ProductComponent);
    component = fixture.componentInstance;
    loader = TestbedHarnessEnvironment.loader(fixture);
    dialog = TestBed.inject(MatDialog);

    store = TestBed.inject(Store);

    component.product = mockData.data[0];

    storageServiceSpy.isTokenExpired.and.returnValue(false);
    alertServiceSpy.notify.and.returnValue(of(EMPTY));

    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeDefined();
  });

  it('should dispatch an action for adding products to the cart', async () => {
    spyOn(dialog, 'open').and.returnValue({ afterClosed: () => of(5) } as MatDialogRef<
      CartDialog,
      any
    >);

    const body = {
      data: {
        items: [
          {
            product_variant_id: component.product.master?.id,
            quantity: 5,
          },
        ],
      },
    };

    spyOn(store, 'dispatch').and.callThrough();

    const addToCartButton = await loader.getHarness(MatButtonHarness);
    await addToCartButton.click();

    expect(store.dispatch).toHaveBeenCalledWith(CartActions.addProductToCart({ body }));
  });

  it('should show an alert to the user that he has to log in before adding products to the cart', async () => {
    storageServiceSpy.isTokenExpired.and.returnValue(true);
    spyOn(dialog, 'open');

    const addToCartButton = await loader.getHarness(MatButtonHarness);
    await addToCartButton.click();

    expect(dialog.open).not.toHaveBeenCalled();
    expect(alertServiceSpy.notify).toHaveBeenCalled();
  });
});
