import { AppState } from './../../../app.reducer';
import { MatButtonHarness } from '@angular/material/button/testing';
import { JwtModule } from '@auth0/angular-jwt';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SharedModule } from 'src/app/shared/shared.module';
import { HarnessLoader } from '@angular/cdk/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { Store, StoreModule } from '@ngrx/store';
import { appReducers } from 'src/app/app.reducer';
import { EffectsModule } from '@ngrx/effects';
import { EffectsArray } from '../../store/effects';
import { ProductsService } from '../../services/products.service';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDialog, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import * as mockCart from '../../../test/mock-cart.json';
import { Meta } from '../../interfaces/meta';
import { CartItemComponent } from './cart-item.component';
import { Cart } from '../../interfaces/cart';
import { of } from 'rxjs';
import { DeleteDialog } from './delete-dialog';
import * as CartActions from '../../store/actions/cart.actions';

describe('CartItemComponent', () => {
  let component: CartItemComponent;
  let fixture: ComponentFixture<CartItemComponent>;
  let loader: HarnessLoader;
  let mockData: { data: Cart; meta: Meta } = (mockCart as any).default;
  let dialog: MatDialog;
  let store: Store<AppState>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CartItemComponent],
      imports: [
        BrowserAnimationsModule,
        SharedModule,
        HttpClientTestingModule,
        JwtModule.forRoot({ config: {} }),
        RouterTestingModule,
        StoreModule.forRoot(appReducers),
        EffectsModule.forRoot(EffectsArray),
        MatTooltipModule,
        MatDialogModule,
      ],
      providers: [ProductsService],
    }).compileComponents();

    fixture = TestBed.createComponent(CartItemComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;

    dialog = TestBed.inject(MatDialog);
    store = TestBed.inject(Store);

    fixture.detectChanges();

    component.cartProduct = mockData.data.items[0];

    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeDefined();
  });

  it('should dispatch an action for deleting the item from the cart', async () => {
    spyOn(dialog, 'open').and.returnValue({ afterClosed: () => of(true) } as MatDialogRef<
      DeleteDialog,
      any
    >);

    spyOn(store, 'dispatch').and.callThrough();

    const body = {
      data: {
        items: [
          {
            id: component.cartProduct.id,
            _destroy: true,
          },
        ],
      },
    };

    const deleteButton = await loader.getHarness(MatButtonHarness);
    await deleteButton.click();

    fixture.detectChanges();

    expect(store.dispatch).toHaveBeenCalledWith(CartActions.deleteItemFromCart({ body }));
  });
});
