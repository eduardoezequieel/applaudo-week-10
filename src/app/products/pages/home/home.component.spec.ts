import { MatButtonHarness } from '@angular/material/button/testing';
import { JwtModule } from '@auth0/angular-jwt';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SharedModule } from 'src/app/shared/shared.module';
import { HarnessLoader } from '@angular/cdk/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home.component';
import { RouterTestingModule } from '@angular/router/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { StoreModule } from '@ngrx/store';
import { appReducers } from 'src/app/app.reducer';
import { EffectsModule } from '@ngrx/effects';
import { EffectsArray } from '../../store/effects';
import { ProductsService } from '../../services/products.service';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDialogModule } from '@angular/material/dialog';
import { By } from '@angular/platform-browser';
import * as mockProducts from '../../../test/mock-products.json';
import { Product } from '../../interfaces/product';
import { Meta } from '../../interfaces/meta';
import { ProductComponent } from '../../components/product/product.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let loader: HarnessLoader;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HomeComponent, ProductComponent],
      imports: [
        BrowserAnimationsModule,
        SharedModule,
        HttpClientTestingModule,
        JwtModule.forRoot({ config: {} }),
        RouterTestingModule,
        StoreModule.forRoot(appReducers),
        EffectsModule.forRoot(EffectsArray),
        MatMenuModule,
        MatPaginatorModule,
        MatTooltipModule,
        MatDialogModule,
      ],
      providers: [ProductsService],
    }).compileComponents();

    fixture = TestBed.createComponent(HomeComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;

    fixture.detectChanges();

    let mockData: { data: Product[]; meta: Meta } = (mockProducts as any).default;
    component.products = mockData.data;

    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeDefined();
  });

  it('should have 10 instances of ProductComponent', async () => {
    const elements = fixture.debugElement.queryAll(By.css('app-product'));
    expect(elements.length).toBe(10);
  });
});
