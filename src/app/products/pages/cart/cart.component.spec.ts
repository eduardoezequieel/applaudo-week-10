import { MatButtonHarness } from '@angular/material/button/testing';
import { of } from 'rxjs';
import { HarnessLoader } from '@angular/cdk/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { By, Meta } from '@angular/platform-browser';
import { Store, StoreModule } from '@ngrx/store';
import { appReducers, AppState } from 'src/app/app.reducer';
import * as mockCart from '../../../test/mock-cart.json';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatMenuModule } from '@angular/material/menu';
import { MatTooltipModule } from '@angular/material/tooltip';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { JwtModule } from '@auth0/angular-jwt';
import { EffectsModule } from '@ngrx/effects';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProductsService } from '../../services/products.service';
import { EffectsArray } from '../../store/effects';
import { CartComponent } from './cart.component';
import { Cart } from '../../interfaces/cart';
import { CartItemComponent } from '../../components/cart-item/cart-item.component';
import { CurrencyPipe } from '@angular/common';
import { DeleteDialog } from '../../components/cart-item/delete-dialog';
import * as CartActions from '../../store/actions/cart.actions';

describe('CartComponent', () => {
  let component: CartComponent;
  let fixture: ComponentFixture<CartComponent>;
  let loader: HarnessLoader;
  let mockData: { data: Cart; meta: Meta } = (mockCart as any).default;
  let dialog: MatDialog;
  let store: Store<AppState>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CartComponent, CartItemComponent],
      imports: [
        BrowserAnimationsModule,
        HttpClientTestingModule,
        JwtModule.forRoot({ config: {} }),
        RouterTestingModule,
        StoreModule.forRoot(appReducers),
        EffectsModule.forRoot(EffectsArray),
        SharedModule,
        MatMenuModule,
        MatTooltipModule,
        MatDialogModule,
      ],
      providers: [ProductsService, CurrencyPipe],
    }).compileComponents();

    fixture = TestBed.createComponent(CartComponent);
    component = fixture.componentInstance;
    loader = TestbedHarnessEnvironment.loader(fixture);

    dialog = TestBed.inject(MatDialog);
    store = TestBed.inject(Store);

    fixture.detectChanges();

    component.cart = mockData.data;

    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeDefined();
  });

  it('should display a list of products in the cart', () => {
    const elements = fixture.debugElement.queryAll(By.css('app-cart-item'));
    expect(elements.length).toBe(mockData.data.items.length);
  });

  it('should check that the total is the same as in the mocked cart', async () => {
    const element = fixture.debugElement.query(By.css('.footer > h1 > b'));
    expect(element.nativeElement.innerText).toBe(
      new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(
        mockData.data.total!
      )
    );
  });

  it('should dispatch an action for deleting the cart', async () => {
    spyOn(dialog, 'open').and.returnValue({ afterClosed: () => of(true) } as MatDialogRef<
      DeleteDialog,
      any
    >);

    spyOn(store, 'dispatch').and.callThrough();

    const deleteCartButton = await loader.getHarness(
      MatButtonHarness.with({ selector: "button[matTooltip='Delete cart']" })
    );
    await deleteCartButton.click();

    expect(store.dispatch).toHaveBeenCalledWith(CartActions.deleteCart());
  });
});
