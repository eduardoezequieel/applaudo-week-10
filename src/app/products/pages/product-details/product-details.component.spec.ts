import { HarnessLoader } from '@angular/cdk/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { By, Meta } from '@angular/platform-browser';
import { Store, StoreModule } from '@ngrx/store';
import { appReducers, AppState } from 'src/app/app.reducer';
import { Product } from '../../interfaces/product';
import * as mockProducts from '../../../test/mock-products.json';
import { ProductDetailsComponent } from './product-details.component';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatMenuModule } from '@angular/material/menu';
import { MatTooltipModule } from '@angular/material/tooltip';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { JwtModule } from '@auth0/angular-jwt';
import { EffectsModule } from '@ngrx/effects';
import { StorageService } from 'src/app/core/storage.service';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProductsService } from '../../services/products.service';
import { EffectsArray } from '../../store/effects';
import { of } from 'rxjs';
import { CartDialog } from '../../components/product/cart-dialog';
import { MatButtonHarness } from '@angular/material/button/testing';
import * as CartActions from '../../store/actions/cart.actions';

describe('ProductDetailsComponent', () => {
  let component: ProductDetailsComponent;
  let fixture: ComponentFixture<ProductDetailsComponent>;
  let loader: HarnessLoader;
  let mockData: { data: Product[]; meta: Meta } = (mockProducts as any).default;
  let dialog: MatDialog;
  let store: Store<AppState>;

  const storageServiceSpy = jasmine.createSpyObj('StorageService', ['isTokenExpired']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProductDetailsComponent],
      imports: [
        BrowserAnimationsModule,
        HttpClientTestingModule,
        JwtModule.forRoot({ config: {} }),
        RouterTestingModule,
        StoreModule.forRoot(appReducers),
        EffectsModule.forRoot(EffectsArray),
        SharedModule,
        MatMenuModule,
        MatTooltipModule,
        MatDialogModule,
      ],
      providers: [ProductsService, { provide: StorageService, useValue: storageServiceSpy }],
    }).compileComponents();

    fixture = TestBed.createComponent(ProductDetailsComponent);
    component = fixture.componentInstance;
    loader = TestbedHarnessEnvironment.loader(fixture);
    dialog = TestBed.inject(MatDialog);

    store = TestBed.inject(Store);

    storageServiceSpy.isTokenExpired.and.returnValue(false);

    fixture.detectChanges();

    component.product = mockData.data[0];
  });

  it('should create the component', () => {
    expect(component).toBeDefined();
  });

  it('should display mocked information', () => {
    const elements = fixture.debugElement.query(By.css('.content')).children;

    elements.forEach((element) => {
      expect(element.nativeElement.innerText).toBeDefined();
    });

    expect(component.product).toBe(mockData.data[0]);
  });

  it('should dispatch an action for adding products to the cart', async () => {
    const elements = fixture.debugElement.query(By.css('.content')).children;

    elements.forEach((element) => {
      expect(element.nativeElement.innerText).toBeDefined();
    });

    spyOn(dialog, 'open').and.returnValue({ afterClosed: () => of(5) } as MatDialogRef<
      CartDialog,
      any
    >);

    const body = {
      data: {
        items: [
          {
            product_variant_id: component.product.master?.id,
            quantity: 5,
          },
        ],
      },
    };

    spyOn(store, 'dispatch').and.callThrough();

    const addToCartButton = await loader.getHarness(MatButtonHarness);
    await addToCartButton.click();

    expect(store.dispatch).toHaveBeenCalledWith(CartActions.addProductToCart({ body }));
  });
});
