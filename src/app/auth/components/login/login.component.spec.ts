import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { MatInputHarness } from '@angular/material/input/testing';
import { MatButtonHarness } from '@angular/material/button/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LoginComponent } from './login.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthService } from 'src/app/core/auth.service';
import { Router } from '@angular/router';
import { EMPTY, of, throwError } from 'rxjs';
import { AlertService } from 'src/app/shared/services/alert.service';

const authServiceSpy = jasmine.createSpyObj('LoginService', ['login']);
const alertServiceSpy = jasmine.createSpyObj('AlertService', ['notify']);

describe('LoginComponent', () => {
  let component: LoginComponent;
  let loader: HarnessLoader;
  let fixture: ComponentFixture<LoginComponent>;
  let router: Router;

  const addCredentials = (password: string) => {
    component.form.controls['email'].setValue('trainee1@example.com');
    component.form.controls['password'].setValue(password);
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [BrowserAnimationsModule, ReactiveFormsModule, SharedModule, RouterTestingModule],
      providers: [
        { provide: AuthService, useValue: authServiceSpy },
        { provide: AlertService, useValue: alertServiceSpy },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(LoginComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    router = TestBed.inject(Router);
  });

  it('should create the component', () => {
    expect(component).toBeDefined();
  });

  it('should check that component contains 2 inputs', async () => {
    const inputs = await loader.getAllHarnesses(MatInputHarness);
    expect(inputs.length).toBe(2);
  });

  it('should verify the validation of the form', () => {
    component.form.get('email')!.setValue('invalid email');

    expect(component.form.invalid).toBeTrue();

    addCredentials('1234');

    expect(component.form.valid).toBeTrue();
  });

  it('should check if login button is disabled depending on form status', async () => {
    const button = await loader.getHarness(MatButtonHarness.with({ text: 'Continue' }));

    expect(await button.isDisabled()).toBeTrue();
    expect(component.form.invalid).toBeTrue();

    addCredentials('1234');

    expect(await button.isDisabled()).not.toBeTrue();
    expect(component.form.valid).toBeTrue();
  });

  it('should do a complete successful login simulation', async () => {
    const mockResponse = {
      data: {
        token: 'asdjsakdlkajsd',
        user: {
          id: 1,
          email: 'trainee1@example.com',
          name: 'Trainee 1',
        },
      },
    };

    authServiceSpy.login.and.returnValue(of(mockResponse));
    alertServiceSpy.notify.and.returnValue(of(EMPTY));
    spyOn(router, 'navigateByUrl');

    addCredentials('Trainee$1');

    const button = await loader.getHarness(MatButtonHarness.with({ text: 'Continue' }));
    await button.click();

    expect(authServiceSpy.login).toHaveBeenCalled();
    expect(alertServiceSpy.notify).toHaveBeenCalled();
    expect(router.navigateByUrl).toHaveBeenCalledOnceWith('/products/home');
  });

  it('should do an invalid login simulation', async () => {
    const mockError = { expected: true, message: 'Invalid credentials' };

    spyOn(router, 'navigateByUrl');
    authServiceSpy.login.and.returnValue(throwError(() => of(mockError)));
    alertServiceSpy.notify.and.returnValue(of(EMPTY));

    addCredentials('1234');

    const button = await loader.getHarness(MatButtonHarness.with({ text: 'Continue' }));
    await button.click();

    expect(authServiceSpy.login).toHaveBeenCalled();
    expect(alertServiceSpy.notify).toHaveBeenCalled();
    expect(router.navigateByUrl).not.toHaveBeenCalled();
  });
});
